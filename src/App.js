// import logo from './logo.svg';
import './App.css';
import MainLayout from "./layout/mainLayout";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";


function App() {
  return (

        <Router>
            <Provider store={store}>
                <div className="App">
                    <MainLayout></MainLayout>
                </div>
            </Provider>

        </Router>



  );
}

export default App;
