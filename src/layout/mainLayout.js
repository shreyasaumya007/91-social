import React, {Component} from 'react';
import NavBar from "../components/navBar";
import {Container, Grid} from "@material-ui/core";
import CardComponent from "../components/cardComponent";
import SpacexHistory from "../pages/spacex_history";

import { Switch, Route } from "react-router-dom";
import SpacexPayloads from "../pages/spacex_payloads";


class MainLayout extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <NavBar></NavBar>
                <Container maxWidth="sm">
                            <Switch>
                                <Route path={'/history'} exact component={SpacexHistory}/>
                                <Route path={'/payload'} component={SpacexPayloads}/>
                            </Switch>
                </Container>

            </div>
        );
    }
}



export default MainLayout;
