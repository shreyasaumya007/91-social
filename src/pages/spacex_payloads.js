import React, {Component} from 'react';
import axios from "axios"
import spacexlogo from "../static/images/spacexlogo.jpg";
import {List} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {CallMade, ExpandLess, ExpandMore, StarBorder} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import CardComponent from "../components/cardComponent";

var response=[];
var DataPerPage= 5;

class SpacexPayloads extends Component {
    constructor(props) {
        super(props);
        this.state={
            currentPage:1,
            query:""
        }
    }
    handlePayloadSearch=(event)=>{
        this.setState({
            query:event.target.value
        })
        console.log("query", this.state.query);


        if( this.state.query.length<0|| this.state.query===""){
            response=response;
            return response
        }
        else if (response.length>0){
            response= response.filter((q)=>{
                return  q.payload_id.toLowerCase().includes(this.state.query.toLowerCase());
            })
        }
        else if (response.filter((q)=>{
            return q.payload_id.toLowerCase()!==(this.state.query);
        })){
            return []
        }
        else {
            return response
        }
    }
    handleClick=(event) =>{

        this.setState({
            currentPage: Number(event.target.id)
        });
    }
    componentDidMount() {
        axios.get('https://api.spacexdata.com/v3/payloads').then(res=>{
            for (let i=0;i<res.data.length;i++){
                response[i]=res.data[i]
                console.log('payload res',response[i].customers)

            }

        })
}

    render() {
        const { currentPage } = this.state;
        const indexOfLastData = currentPage * DataPerPage;
        const indexOfFirstData = indexOfLastData - DataPerPage;
        const currentTodos = response.slice(indexOfFirstData, indexOfLastData);
        const renderTodos = currentTodos.map((payload, index) => {
            return (

                <CardComponent propsValue={
                    <div >
                         <span>
                        <img alt="SpaceXLogo" src={spacexlogo} style={{verticalAlign: 'middle',
                            width: '40px',
                            height: '40px',
                            borderRadius: '50%'}}/><b >{payload.payload_id}</b>
                            <span >
                            </span>
                        </span>
                        <div>customer: {payload.customers}</div>
                        <div>nationality: {payload.nationality}</div>
                        {payload.manufacturer?<div style={{margin:'20px'}}>manufacturer: {payload.manufacturer}</div>:''
                        }
                        {payload.payload_mass_kg?<div>Kg: {payload.payload_mass_kg }</div>:''
                        }

                    </div>
                }
                >
                </CardComponent>
            )
        });
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(response.length / DataPerPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (

                <span>
                    <button  style={{width:'50px'}} key={number}
                             id={number}
                             onClick={this.handleClick}>
                        {number}
                    </button>

                </span>
            );
        });
        return(
            <div>
                <input  type={"text"} placeholder={"search..."} style={{width:'200px',height:'30px',
                    color:'white',margin:'20px',background:'#262626'}} onChange={this.handlePayloadSearch}/>

                <ul>
                    {renderTodos}
                </ul>
                <ul id="page-numbers">
                    page no:{renderPageNumbers}
                </ul>
            </div>
        )
    }
}

SpacexPayloads.propTypes = {};

export default SpacexPayloads;
