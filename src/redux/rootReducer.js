import {combineReducers} from "redux";
import historyReducer from './space_x_history/history_reducer'
const rootReducer = combineReducers(
    {
        space_x_history:historyReducer
    }
)

export default rootReducer
