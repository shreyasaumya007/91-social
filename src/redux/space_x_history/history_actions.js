import {FETCH_SPACE_X_FAILURE, FETCH_SPACE_X_REQUEST, FETCH_SPACE_X_SUCCESS} from "./history_type";
import axios from "axios";
export const fetchSpacexHistoryRequest=()=>{
    return{
        type:FETCH_SPACE_X_REQUEST
    }
}
export const fetchSpacexHistorySuccess=(historyResponse)=>{
    return{
        type:FETCH_SPACE_X_SUCCESS,
        payload:historyResponse
    }
}
export const fetchSpacexHistoryFailure=(error)=>{
    return{
        type:FETCH_SPACE_X_FAILURE,
        payload:error
    }
}

export const fetchHistory=()=>{
    return (dispatch)=>{
        dispatch(fetchSpacexHistoryRequest)
        axios.get('https://api.spacexdata.com/v3/history').
        then(response=>{
            const historyResponse=response.data
            dispatch(fetchSpacexHistorySuccess(historyResponse))
        }).catch(error=>{
            const errorMsg=error.message
            dispatch(fetchSpacexHistoryFailure(errorMsg))
        })
    }
}
