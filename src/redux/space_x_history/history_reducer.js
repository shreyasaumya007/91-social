import {FETCH_SPACE_X_FAILURE, FETCH_SPACE_X_REQUEST, FETCH_SPACE_X_SUCCESS} from "./history_type";

const initialState={
    loading:false,
    historyResponse:[],
    error:''
}
const reducer = (state=initialState,action)=>{
switch (action.type){
    case FETCH_SPACE_X_REQUEST:
        return{
            ...state,
            loading: true
        }
    case FETCH_SPACE_X_SUCCESS:
        return {
            ...state,
            historyResponse: action.payload,
            error: ''
        }
    case FETCH_SPACE_X_FAILURE:
        return {
            ...state,
            historyResponse: [],
            error: action.payload
        }
    default: return state
}
}

export default reducer
